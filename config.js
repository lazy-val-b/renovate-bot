module.exports = {
    platform: 'gitlab',
    gitAuthor: 'Renovate Bot <renovate@gitlab.dev.si.usi.ch>',
    labels: ['renovate'],
    onboarding: false,
    autodiscover: true,
    requireConfig: true
  };
